// SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#include <QQmlEngine>
#include <QResource>

#include "filetagging_plugin.h"

#include "tagslist.h"
#include "tagging.h"

void FileTaggingPlugin::registerTypes(const char *uri)
{
#if defined(Q_OS_ANDROID)
    QResource::registerResource(QStringLiteral("assets:/android_rcc_bundle.rcc"));
#endif

    qmlRegisterSingletonType<Tagging>(uri, 1, 3, "Tagging", [](QQmlEngine *engine, QJSEngine *scriptEngine) -> QObject * {
        Q_UNUSED(engine)
        Q_UNUSED(scriptEngine)
        return Tagging::getInstance();
    }); //the singleton instance results in having tagging instance created in different threads which is not supported byt the slq driver
    
    qmlRegisterType<TagsList>(uri, 1, 0, "TagsListModel");
    qmlRegisterType(resolveFileUrl(QStringLiteral("private/TagList.qml")), uri, 1, 0, "TagList");
    qmlRegisterType(resolveFileUrl(QStringLiteral("TagsBar.qml")), uri, 1, 0, "TagsBar");
    qmlRegisterType(resolveFileUrl(QStringLiteral("TagsDialog.qml")), uri, 1, 0, "TagsDialog");
    qmlRegisterType(resolveFileUrl(QStringLiteral("NewTagDialog.qml")), uri, 1, 3, "NewTagDialog");
}

void FileTaggingPlugin::initializeEngine(QQmlEngine *engine, const char *uri)
{
    Q_UNUSED(uri);
  
}
